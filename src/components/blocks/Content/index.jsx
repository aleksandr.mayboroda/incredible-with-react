import React from 'react'

import './content.scss'

import Welcome from '../Welcome'

import Footer from '../Footer'

import SectionHeading from '../SectionHeading'
import SectionSomeText from '../SectionSomeText'
import SectionLightBox from '../SectionLightBox'
import SectionContactForm from '../SectionContactForm'

const Content = () => {
  return (
    <div className="content">
      <Welcome />
      <SectionHeading />
      <SectionSomeText />
      <SectionLightBox />
      <SectionContactForm />
      <Footer />
    </div>
  )
}

export default Content
