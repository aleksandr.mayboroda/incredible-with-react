import React from 'react'

import './welcome.scss'

import Button from '../../chunks/Button'

const Welcome = () => {
  return (
    <section
      className="welcome"
      style={{
        backgroundImage: 'url(./image/sections/photo.png)',
      }}
    >
      <div className="welcome__content">
        <h2 className="welcome__title">Welcome to the <br/> incredible</h2>
        <p className="welcome__subtitle">
          This is some text inside of a div block.
        </p>
        <div className="welcome__buttons">
          <Button text="button text" hover={true} className="welcome__btn" />
          <Button text="button text" hover={true} className="welcome__btn" />
        </div>
      </div>
    </section>
  )
}

export default Welcome
