import { useState, useEffect } from 'react'
import './menu.scss'

import Logo from '../../chunks/Logo'

const linksList = [
  { name: 'home', link: '#' },
  { name: 'about', link: '#' },
  { name: 'gallery', link: '#' },
  { name: 'contact', link: '#' },
]

const Menu = () => {
  const [links, setLinks] = useState('')

  useEffect(() => {
    if (linksList.length > 0) {
      let newLinks = linksList.map(({ name, link }) => (
        <li className="menu__item" key={name}>
          <a className="menu__link" href={link}>
            {name}
          </a>
        </li>
      ))
      setLinks(newLinks)
    }
  }, [])

  return (
    <div className="menu">
      <Logo />
      <ul className="menu__list">
        {links}
      </ul>
    </div>
  )
}

export default Menu
