import React from 'react'

import './sectionHeading.scss'

import SectionHeader from '../../chunks/SectionHeader'
import ServicesList from '../ServicesList'

const SectionHeading = () => {
  return (
    <section className="section_heading section_standart">
      <div className="container__inner">
        <SectionHeader
          titleText="section heading"
          subTitleText="This is some text inside of a div block."
        />
        <ServicesList />
      </div>
    </section>
  )
}

export default SectionHeading
