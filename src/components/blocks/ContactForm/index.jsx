import {useState} from 'react'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import './contactForm.scss'
import '../../chunks/Form/form.scss'

import TextField from '../../chunks/Form/TextField'
import TextareaField from '../../chunks/Form/TextareaField'
import Button from '../../chunks/Button'

import {ContactFormSchema} from '../../chunks/Form/schemas'

const ContactForm = () => {
  const [formResultMessage, setFormResultMessage] = useState(null)
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(ContactFormSchema),
    mode: 'onChange'
  })

  const onSubmit = (data) => {
    console.log(data)
    setFormResultMessage('we will contact you soon...')
    reset()
    setTimeout(() => {setFormResultMessage(null)}, 5000)
  }

  return (
    <form className="contact_form" onSubmit={handleSubmit(onSubmit)}>
      <TextField
        name="name"
        placeholder="name"
        errors={errors}
        register={register}
      />
      <TextField
        name="email"
        type="email"
        placeholder="email"
        errors={errors}
        register={register}
      />

      <TextareaField
        name="message"
        placeholder="message"
        rows="5"
        blockClassName="contact_form__full"
        errors={errors}
        register={register}
      />

      {formResultMessage && <p className='form__success contact_form__full'>{formResultMessage}</p>}

      <Button className="contact_form__submit contact_form__full" text="sent" />
    </form>
  )
}

export default ContactForm
