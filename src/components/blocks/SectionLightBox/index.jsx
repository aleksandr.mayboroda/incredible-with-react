import React from 'react'

import './sectionLightBox.scss'

import SectionHeader from '../../chunks/SectionHeader'
import LightboxList from '../LightboxList'

const SectionLightBox = () => {
  return (
    <section className="section_lightbox section_standart">
      <div className="container__inner">
        <SectionHeader
          titleText="Lightbox Section"
          subTitleText="This is some text inside of a div block."
        />
        <LightboxList />
      </div>
    </section>
  )
}

export default SectionLightBox
