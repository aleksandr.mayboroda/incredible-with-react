import React from 'react'

import './sectionSomeText.scss'

import Button from '../../chunks/Button'

const SectionSomeText = () => {
  return (
    <section className="section_some_text">
      <div className="container__inner section_some_text__inner">
        <p className='section_some_text__text'>Here you can put some text</p>
        <Button text = 'Call To Action' hover = {false} />
      </div>
    </section>
  )
}

export default SectionSomeText