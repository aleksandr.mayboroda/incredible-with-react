import { useState, useEffect } from 'react'

import './lightboxList.scss'

import LightboxItem from '../../chunks/LightboxItem'

const lightboxList = [
  { title: 'lightbox1', path: './image/lightbox/lightbox1.png', link: '#' },
  { title: 'lightbox2', path: './image/lightbox/lightbox2.png', link: '#' },
  { title: 'lightbox3', path: './image/lightbox/lightbox3.png', link: '#' },
  { title: 'lightbox4', path: './image/lightbox/lightbox4.png', link: '#' },
  { title: 'lightbox5', path: './image/lightbox/lightbox5.png', link: '#' },
  { title: 'lightbox6', path: './image/lightbox/lightbox6.png', link: '#' },
  { title: 'lightbox7', path: './image/lightbox/lightbox7.png', link: '#' },
  { title: 'lightbox8', path: './image/lightbox/lightbox8.png', link: '#' },
  // { title: 'lightbox9', path: './image/lightbox/lightbox9.png', link: '#' },
]

const LightboxList = () => {
  const [lightBoxes, setLightBoxes] = useState(null)

  useEffect(() => {
    if (lightboxList.length > 0) {
      const newLightBoxes = lightboxList.map(({ title, path, link }) => (
        <li className="lightbox_list__item" key={title}>
          <LightboxItem title={title} path={path} link={link} /> 
        </li>
      ))
      setLightBoxes(newLightBoxes)
    }
  }, [])

  return <ul className="lightbox_list">{lightBoxes}</ul>
}

export default LightboxList
