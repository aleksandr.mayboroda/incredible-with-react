import React from 'react'

import './sectionContactForm.scss'

import SectionHeader from '../../chunks/SectionHeader'
import ContactForm from '../ContactForm'

const SectionContactForm = () => {
  return (
    <section className="section_standart section_contact_form">
      <div className="container__inner">
        <SectionHeader titleText='Contact Form' subTitleText='This is some text inside of a div block.' />
        <ContactForm />
      </div>
    </section>
  )
}

export default SectionContactForm
