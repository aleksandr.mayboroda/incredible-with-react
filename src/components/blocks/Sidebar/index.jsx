import React from 'react'

import './sidebar.scss'

import Menu from '../Menu'
import Social from '../Social'

const Sidebar = () => {
  return (
    <div className="sidebar">
      <Menu />
      <Social />
    </div>
  )
}

export default Sidebar
