import { useState, useEffect } from 'react'
// import Icon from '../../chunks/Icon'

import './serviceList.scss'

import ServiceItem from '../../chunks/ServiceItem'

const servicesList = [
  {
    type: 'camera',
    title: 'Style 1',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.',
    link: '#',
  },
  {
    type: 'window',
    title: 'Style 2',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.',
    link: '#',
  },
  {
    type: 'gear',
    title: 'Style 3',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.',
    link: '#',
  },
  {
    type: 'cloud',
    title: 'Style 4',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.',
    link: '#',
  },
]

const ServicesList = () => {
  const [services, setServices] = useState(null)

  useEffect(() => {
    if (servicesList.length > 0) {
      const newServices = servicesList.map(({ type, title, text, link }) => (
        <li className="service_list__item" key={title}>
          <ServiceItem type={type} title={title} text={text} link={link} />
        </li>
      ))
      setServices(newServices)
    }
  }, [])

  return (
    <ul className="service_list">
     {services}
    </ul>
  )
}

export default ServicesList
