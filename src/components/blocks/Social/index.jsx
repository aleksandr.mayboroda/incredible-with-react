import { useState, useEffect } from 'react'

import './social.scss'

import Icon from '../../chunks/Icon'

const socialLinks = [
  { name: 'facebook', link: '#' },
  { name: 'twitter', link: '#' },
  { name: 'ball', link: '#' },
  { name: 'be', link: '#' },
  { name: 'w', link: '#' },
  { name: 'instagram', link: '#' },
  { name: 'mail', link: '#' },
]

const Social = () => {
  const [links, setLinks] = useState(null)

  useEffect(() => {
    if (socialLinks.length > 0) {
      let newLinks = socialLinks.map(({ name, link }) => (
        <li className="social__item" key={name}>
          <a className="social__link" href={link}>
            <Icon type={name} filled={true} width="17px" height="17px" />
          </a>
        </li>
      ))
      setLinks(newLinks)
    }
  }, [])

  return <ul className="social">{links}</ul>
}

export default Social
