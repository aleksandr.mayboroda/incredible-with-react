import React from 'react'

import './footer.scss'

const Footer = () => {
  return (
    <footer className='footer'>
      <p className='footer__text'>Copyright Incredible. Made in Webflow.</p>
    </footer>
  )
}

export default Footer