import React from 'react'

import './serviceItem.scss'

import Icon from '../Icon'

const ServiceItem = ({ type, title, text, link }) => {
  return (
    <div className="service_item">
      <div className='service_item__icon'>
        <Icon type={type} />
      </div>
      <h5 className="service_item__title">{title}</h5>
      <p className="service_item__text">{text}</p>
      <a className="service_item__link" href={link}>
        Text Link
      </a>
    </div>
  )
}

export default ServiceItem
