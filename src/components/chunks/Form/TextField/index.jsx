import React from 'react'

import '../form.scss'

const TextField = ({
  name = '',
  errors = {},
  blockClassName = '',
  className = '',
  type = 'text',
  placeholder = '',
  register
}) => {
  return (
    <div className={`form__element ${blockClassName}`}>
      <input
        className={`form__field ${className} ${
          errors[name] ? 'form__field_error' : ''
        }`}
        type={type}
        name={name}
        placeholder={placeholder}
        {...register(name)}
      />
      {errors[name] && <p className="contact_form__error">{errors[name]['message']}</p>}
    </div>
  )
}

export default TextField
