import * as yup from "yup";

const errors = {
  required: 'this field is required',
  email: 'please enter correct email'
}

export const ContactFormSchema = yup.object({
  name: yup.string().trim().required(errors.required).min(5),
  email: yup.string().trim().email().required(errors.required),
  message: yup.string().min(10).trim('plz no unneeded spaces!').required(errors.required),
}).required();