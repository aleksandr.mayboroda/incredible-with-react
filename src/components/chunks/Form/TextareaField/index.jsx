import React from 'react'

import '../form.scss'

const TextareaField = ({
  name = '',
  errors={},
  blockClassName = '',
  className = '',
  placeholder = '',
  rows = 1,
  register
}) => {
  return (
    <div className={`form__element ${blockClassName}`}>
      <textarea
        className={`form__field form__textarea ${className} ${
          errors[name] ? 'form__field_error' : ''
        }`}
        name={name}
        placeholder={placeholder}
        rows={rows}
        {...register(name)}
      ></textarea>
      {errors[name] && <p className="form__error">{errors[name]['message']}</p>}
    </div>
  )
}

export default TextareaField
