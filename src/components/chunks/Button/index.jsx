import React from 'react'

import './button.scss'

const Button = ({
  text = 'button',
  className = 'btn',
  hover = false,
  onClick,
}) => {
  return (
    <button
      className={`btn ${className} ${hover ? 'btn_hover' : ''}`}
      onClick={onClick}
    >
      {text}
    </button>
  )
}

export default Button
