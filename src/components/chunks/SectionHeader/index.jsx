import React from 'react'

import './sectionHeader.scss'

const SectionHeader = ({titleText = '', subTitleText = ''}) => {
  return (
    <header className='section_header'>
      <h3 className='section_header__title'>{titleText}</h3>
      <p className='section_header__subtitle'>{subTitleText}</p>
    </header>
  )
}

export default SectionHeader