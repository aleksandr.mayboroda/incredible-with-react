import React from 'react'

import './lightboxItem.scss'

const LightboxItem = ({ title, path, link }) => {
  return (
    <div className="lightbox_item">
      <a className="lightbox_item__link" href={link}>
        <img className="lightbox_item__image" src={path} alt={title} />
      </a>
    </div>
  )
}

export default LightboxItem
