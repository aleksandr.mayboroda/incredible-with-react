import React from 'react'

import './logo.scss'

const Logo = () => {
  return (
    <div className="logo">
      <a className="logo__link" href="/">
        <h1 className="logo__text">incredible</h1>
      </a>
    </div>
  )
}

export default Logo
