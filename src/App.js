import React from 'react'

import Sidebar from './components/blocks/Sidebar'
import Content from './components/blocks/Content'

const App = () => {
  return (
    <div className="container main">
      <Sidebar />
      <Content />
    </div>
  )
}

export default App
