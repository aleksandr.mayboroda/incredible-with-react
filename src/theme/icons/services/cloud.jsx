export const cloud = ({
  color = '#000',
  width = '80',
  height = '60',
  // filled = false,
}) => {
  return (
    <svg width={width} height={height} viewBox="0 0 80 60" fill="none">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M50 0C37 0 26 8.33333 21.6667 20H20C9 20 0 29 0 40C0 51 9 60 20 60H50C66.6667 60 80 46.6667 80 30C80 13.3333 66.6667 0 50 0ZM50 53.3333H20C12.6667 53.3333 6.66667 47.3333 6.66667 40C6.66667 32.6667 12.6667 26.6667 20 26.6667H27C28.6667 15.3333 38.3333 6.66667 50 6.66667C63 6.66667 73.3333 17 73.3333 30C73.3333 43 63 53.3333 50 53.3333Z"
        fill={color}
      />
    </svg>
  )
}
